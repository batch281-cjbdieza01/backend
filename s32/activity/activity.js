const http = require("http");

// Mock database
let courses = [
  {
    id: 1,
    name: "HTML",
  },
  {
    id: 2,
    name: "CSS",
  },
];

let port = 4000;
let app = http.createServer(function (request, response) {
  // Route for returning all courses upon receiving a GET request
  if (request.url === "/courses" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify(courses));
    response.end();
  }
  // Route for adding a new course upon receiving a POST request
  if (request.url === "/courses" && request.method === "POST") {
    let requestBody = "";

    request.on("data", function (data) {
      requestBody += data;
    });

    request.on("end", function () {
      requestBody = JSON.parse(requestBody);

      let newCourse = {
        id: courses.length + 1,
        name: requestBody.name,
      };

      courses.push(newCourse);

      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(newCourse));
      response.end();
    });
  }
  // Route for updating a course upon receiving a PUT request
  if (request.url.startsWith("/courses/") && request.method === "PUT") {
    const courseId = parseInt(request.url.split("/")[2]);

    const index = courses.findIndex((course) => course.id === courseId);

    if (index !== -1) {
      let requestBody = "";

      request.on("data", function (data) {
        requestBody += data;
      });

      request.on("end", function () {
        requestBody = JSON.parse(requestBody);

        courses[index].name = requestBody.name;

        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(JSON.stringify(courses[index]));
        response.end();
      });
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Course not found");
      response.end();
    }
  }
  // Route for deleting a course upon receiving a DELETE request
  if (request.url.startsWith("/courses/") && request.method === "DELETE") {
    const courseId = parseInt(request.url.split("/")[2]);

    const index = courses.findIndex((course) => course.id === courseId);

    if (index !== -1) {
      const deletedCourse = courses.splice(index, 1)[0];

      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(deletedCourse));
      response.end();
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Course not found");
      response.end();
    }
  }
});

app.listen(port, () => console.log("Server running at localhost:4000"));
