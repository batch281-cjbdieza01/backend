//Add code here




const http = require("http");
const port = 4000;

const app = http.createServer((request, response) => {
  if (request.url === "/" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to Booking System");
  } else if (request.url === "/profile" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to your profile!");
  } else if (request.url === "/courses" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Here’s our courses available");
  } else if (request.url === "/addcourse" && request.method === "POST") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Add a course to our resources");
  } else if (request.url === "/updatecourse" && request.method === "PUT") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Update a course to our resources");
  } else if (request.url === "/archivecourses" && request.method === "DELETE") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Archive courses to our resources");
  } else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end("404 Not Found");
  }
});

app.listen(port, () => console.log("Server is running at localhost:4000"));


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
