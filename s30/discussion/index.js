// Aggregation
// Create documents for fruits database
db.fruits.insertMany([
    {
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id" : 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id" : 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id" : 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);

// MongoDB Aggregation
/*
 - Used to generate manipulate data and perform operaions to create filtered results

 Using  Aggregate methods
 - "$match" - used to pass documents that meet specified conditions
 - Syntax
    - { $match: { field: value}}

- $group - used to group elements together and field-value pairs using tha data from the grouped elements
- Syntax
    db.collectionName.aggregate([
        { $match: { field: value } },
        { $group: { _id: "$field" }, { result: { operation } } }
    ])
*/
db.fruits.aggregate([
    { $match: { "onSale": true }},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
])
// Field Projection with aggregation
/*
 &project - used when aggregating data to include/exclude fields from the returned results
Syntax:
    - { $ project : { field: 1/0 } }

*/
db.fruits.aggregate([ 
    { $match: { "onSale": true }},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } }
])

// Sorting aggregated results
/*
    $sort - used to change the order agregated results
    Syntax:
        { $sort: { field: 1/-1 } }
*/
db.fruits.aggregate([ 
    { $match: { "onSale": true }},
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $sort: { total: -1 } }
])

// Aggregating results based on array fields
/*
    $unwind - deconstructs an array field from a collection/field with an array value to output a result for each element.
    Syntax:
        { $unwind: field }
*/

db.fruits.aggregate([
    { $unwind: "$origin"}
])

// Displays fruit document by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([ 
    { $unwind: "$origin" },
    { $group: { _id: "$origin", kinds: { $sum: 1 } } }
]);

// One-to-one Relationship
// Create an id and stores it in the variable owner
var owner = ObjectId();

// Creates an "owner" document that uses the generate id
db.owners.insert({ 
    _id: owner,
    name: "John Smith",
    contact: "09871236545"
})

// Change the "<owner_id" using the actual ind in the previouslky created document
db.suppliers.insert({
    name: "ABC fruits",
    contact: "09193219878",
    owner_id: ("646b5b6e846d2a870af9f7af")
})

// One-to-few relationship
db.suppliers.insertOne({
    name: "DEF Fruits",
    contact: "09179873212",
    addresses: [
        { street: "123 San Jose St", city: "Manila" },
        { street: "367 Gil Puya", city: "Makati" }
    ]
});

// One-to-many Relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.supplier.insertOne({
    _id: supplier,
    name: "GHI Fruits",
    contact: "09215012365",
    branches: [
        branch1
    ]
});

db.branches.insertOne({
    _id: branch1,
    name: "BF Homes",
    address: "123 Arcardio Santos St.",
    city: "Panaque",
    suplier_id: ObjectId("646b61f5846d2a870af9f7b2")
})

db.branches.insertOne({
    _id: branch2,
    name: "BF Homes",
    address: "123 Arcardio Santos St.",
    city: "Panaque",
    suplier_id: ObjectId("646b61f5846d2a870af9f7b2")
})

