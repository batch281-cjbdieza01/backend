//////// activity


//// Use the count operator to count the total number of fruits on sale.

db.fruits.count({ "onSale": true }) // studio 3t shows the message that the count method in MongoDB has been deprecated since version 4.0 and removed in version 4.2. In newer versions, then I use the `countDocuments`  

db.fruits.countDocuments({ "onSale": true })
// dosplaying 4 results in the query line

// and i tried aggregate framework and achieve the same same result. 

db.fruits.aggregate([           //same same result
    {
      $match: { "onSale": true }
    },
    {
      $group: {
        _id: null,
        count: { $sum: 1 }
      }
    }
  ])

/*
  {
    "_id" : null,
    "count" : 4.0
}
*/



  //// Use the average operator to get the average price of fruits onSale per supplier.
  db.fruits.aggregate([
    {
      $match: { "onSale": true }
    },
    {
      $group: {
        _id: "$supplier_id",
        averagePrice: { $avg: "$price" }
      }
    }
  ])
// 
/*  
{
    "_id" : NumberInt(2),
    "averagePrice" : 70.0
}
{
    "_id" : NumberInt(1),
    "averagePrice" : 45.0
}
*/




//// Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
    {
      $group: {
        _id: "$supplier_id",
        maxPrice: { $max: "$price" }
      }
    }
  ])
/* result                         
{
    "_id" : NumberInt(2),
    "maxPrice" : NumberInt(120)
}
{
    "_id" : NumberInt(1),
    "maxPrice" : NumberInt(50)
}
*/
//==> https://stackoverflow.com/


////Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
    {
      $group: {
        _id: "$supplier_id",
        minPrice: { $min: "$price" }
      }
    }
  ])
// result
/*
{
    "_id" : NumberInt(1),
    "minPrice" : NumberInt(40)
}
{
    "_id" : NumberInt(2),
    "minPrice" : NumberInt(20)
}
*/
// by simply replacing the `$max` into `$min` from the previous query
