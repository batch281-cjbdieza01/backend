// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
for (let i = 0; i < string.length; i++){
    let currentLetter = string[i];
    if (currentLetter === 'a' || currentLetter === 'e' || currentLetter ==='i' || currentLetter === 'o' || currentLetter === 'u') {
        continue;
    } else {
        filteredString += currentLetter;
    }
}
console.log(filteredString);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}