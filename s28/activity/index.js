// use hotel

db.rooms.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "roomsAvailable": 10,
    "isAvailable": false
})

// Using insert many
db.rooms.insertMany([
    {
        "name": "double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for all family going on a vacation",
        "roomsAvailable": 5,
        "isAvailable": false
    },
    {
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description":"A room with a queen sized bed perfect for a single gateway",
        "roomsAvailable": 15,
        "isAvailable": false
    }
])

// Using find
db.rooms.find({"name": "double"})

// Using update one
db.rooms.updateOne(
{"name": "queen"},
{$set: {"roomsAvailable": 0}}

)

// Using deleteMany
db.rooms.deleteMany ({ "roomsAvailable": 0 })

//https://cloud.mongodb.com/v2/641be403f4ccef646395bc02#/metrics/replicaSet/6465faf512ec3c46d2f964ee/explorer/Hotel/rooms/find