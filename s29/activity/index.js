// Find users with letter "s" in their first name or "d" in their last name:
db.users.find({
    "$or": [
      { "firstName": { "$regex": ".*s.*", "$options": "i" } },
      { "lastName": { "$regex": ".*d.*", "$options": "i" } }
    ]
  })
// Jane Doe and Stephen Hawking showed up in the query line.

  
 // Find users who are from the HR department and their age is greater than or equal to 70:
 db.users.find({
    "$and": [
      { "department": "HR" },
      { "age": { "$gte": 70 } }
    ]
  })
  // The query line showing Stephen Hawking and Niel Armstrong


//  Find users with the letter "e" in their first name and have an age less than or equal to 30:
db.users.find({
    "$and": [
      { "firstName": { "$regex": "e", "$options": "i" } },
      { "age": { "$lte": 30 } }
    ]
  })

  // result Jane Doe
  
