const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://cjbdieza01:0921506497aA@wdc028-course-booking.9cxhwxo.mongodb.net/b281_to_do?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Set notification for connection success or failure
const db = mongoose.connection;

// If a connection error occurred, output it in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a success message in the console
db.once("open", () => console.log("Connected to the MongoDB database"));

// Mongoose Schema
const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  email: String
});

// Model [User model]
const User = mongoose.model("User", userSchema);

// Allows the app to read JSON data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({ extended: true }));

// Create a new user
app.post("/signup", (req, res) => {
  const { username, password, email } = req.body;
  const newUser = new User({ username, password, email });

  newUser
    .save()
    .then(() => {
      return res.status(201).send("User created successfully");
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).send("Failed to create user");
    });
});

// Retrieve all users
app.get("/users", (req, res) => {
    User.find({})
      .then((users) => {
        return res.status(200).json(users);
      })
      .catch((err) => {
        console.error(err);
        return res.status(500).send("Error retrieving user information");
      });
  });
  

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
