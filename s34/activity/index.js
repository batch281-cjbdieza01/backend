const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

app.get("/home", (req, res) => {
  res.send("Welcome to the Home page");
});

let users = [
  { userName: 'Martin123', passWord: 'Kalikak' },
  { userName: 'John321', passWord: 'Smith' },
  { userName: 'Michael012', passWord: 'Johnson' }
];

app.get('/users', (req, res) => {
  res.json(users);
});

app.delete("/delete-user", (req, res) => {
  const userName = req.body.userName;
  let message;

  if (users.length > 0) {
    let userFound = false;
    for (let i = 0; i < users.length; i++) {
      if (users[i].userName === userName) {
        users.splice(i, 1); // Remove the current object from the array
        message = `User ${userName} has been deleted.`;
        userFound = true;
        break;
      }
    }

    if (!userFound) {
      message = "No users found.";
    }
  } else {
    message = "User does not exist.";
  }

  res.send(message);
});
 

if (require.main === module) {
  app.listen(port, () => console.log(`Server is running at port ${port}`));
}

module.exports = app;



// app.delete("/delete-user", (req, res) => {
//   const userName = req.body.userName;

//   // Find the index of the user in the users array
//   const userIndex = users.findIndex(user => user.userName === userName);

//   if (userIndex !== -1) {
//     // Remove the user from the users array
//     const deletedUser = users.splice(userIndex, 1);
//     const deletedUserName = deletedUser[0].userName;
//     res.send(`User ${deletedUserName} has been successfully deleted.`);
//   } else {
//     res.send(`User not found`)
//   }
// });



