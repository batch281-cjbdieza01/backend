console.log("Hello World");

// Create an object called trainer using object literals
const trainer = {};

// Initialize/add the given object properties and methods

// Properties
trainer.name = "Ash";
trainer.age = 20;
trainer.pokemon = [];
trainer.friends = {
  bestFriends: ["Brock", "Misty"],
};

// Methods
trainer.talk = function () {
  console.log("Pikachu! I choose you");
};

// Check if all properties and methods were properly added

// Access object properties using dot notation
console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.pokemon);
console.log(trainer.friends);
console.log(trainer.friends.bestFriends);

// Access object properties using square bracket notation
console.log(trainer["name"]);
console.log(trainer["age"]);
console.log(trainer["pokemon"]);
console.log(trainer["friends"]);
console.log(trainer.friends["bestFriends"]);

// Access the trainer "talk" method
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, type) {
  this.name = name;
  this.type = type;
  this.tackle = function (target) {
    console.log(this.name + " tackles " + target.name + "!");
    target.health -= this.attack; // Subtract attacker's attack from target's health
    console.log(target.name + "'s health is now reduced to " + target.health);
  };
}

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", "Electric");
// Create/instantiate a new pokemon
let charizard = new Pokemon("Charizard", "Fire");
// Create/instantiate a new pokemon
let squirtle = new Pokemon("Squirtle", "Water");
// Invoke the tackle method and target a different object
pikachu.tackle(charizard);
// Invoke the tackle method and target a different object
charizard.tackle(squirtle);

let myPokemon = {
  name: "Pikachu",
  level: 40,
  health: 400,
  attack: 200,
  tackle: function (target) {
    console.log("This Pokemon tackled " + target.name);
    target.health -= this.attack; 
    console.log(target.name + "'s health is now reduced to " + target.health);
  },
  faint: function () {
    console.log("Pokemon fainted");
  },
};
console.log(myPokemon);

// Creating an object constructor
function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  // Methods
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name);
    target.health -= this.attack;
    console.log(target.name + "'s health is now reduced to " + target.health);
  };
  this.faint = function (target) {
    console.log(this.name + " fainted.");
  };
}

// Create new instances of the "Pokemon" object
let Pikachu = new Pokemon("Pikachu", 200);
let Charizard = new Pokemon("Charizard", 180);
let Squirtle = new Pokemon("Squirtle", 150);

Pikachu.tackle(Charizard);
Charizard.tackle(Pikachu);
Squirtle.tackle(Pikachu);
Pikachu.tackle(Squirtle);
Charizard.tackle(Squirtle);





// Do not modify
// For exporting to test.js
// Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== 'undefined' ? trainer : null,
    Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null
  };
} catch (err) {

}
