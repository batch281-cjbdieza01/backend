// JSON Object
/*
 - JSON stands for JavasScript Object Notation
    Syntax:
        {
            "city": "Quezon City",
            "Province": "Metro Manila",
            "Country": "Philipines"
        }
*/
//JSON Arrays
/*
        "cities": [
            {"city": "Quezon city", "province": "Metro Manila", "country": "Philippines" },
            {"city": "Quezon city", "province": "Metro Manila", "country": "Philippines" },
            {"city": "Quezon city", "province": "Metro Manila", "country": "Philippines" }
        ]

// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name, age, breed.

"dogs": [
    {"name": "Tagpi", "age": "14", "breed": "rotweiller"},
    {"name": "Whitey", "age": "6", "breed": "rotweiller"},
    {"name": "Blackey", "age": "7", "breed": "rotweiller"}
]
*/
// JSON Methods

// Convert Data into Stringified JSON
let batchesArr = [{ batchName : 'Batch X' }, { batchName: 'Batch Y' }];

// the "stringify" method is used to convert Javascript objects into a string
console.log('Result of stringify methos:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
    name: 'John',
    age: 31,
    address: {
        city: 'Manila',
        country: 'Philippines'
    }
});
console.log(data);

// Using stringify method with vatiables
// User details 
/*
let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
    city: prompt('Which city do you live in?'),
    country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
    firsName: firstName,
    lastName: lastName,
    age: age,
    address: address
})
console.log(otherData);
*/

// Mini Activity - Create a JSON data that will accept user car detais with variables brand, type, year.

// let carBrand = prompt('What is the brand of your car?');
// let carType = prompt('What is the type of your car?');
// let carYear = prompt('What is the manufacturing date of your car?');

// let carData = JSON.stringify({
//   carBrand: carBrand,
//   carType: carType,
//   carYear: carYear
// });
// console.log(carData);


// Converting stringified JSON into Javascript Objects
let batchesJSON = `[{"batchName": "Batch X"}, { "batchName": "Batch Y" }]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));

