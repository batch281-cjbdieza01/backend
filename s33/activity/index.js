//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo() {
   const response = await fetch('https://jsonplaceholder.typicode.com/posts');
   const json = await response.json();
   return json;
 }
 
 (async () => {
   const result = await getSingleToDo();
   console.log(result);
 })();
 
  // Getting all to do list items
  async function getAllToDo() {
   return await (
     fetch('https://jsonplaceholder.typicode.com/todos')
       .then((response) => response.json())
       .then((json) => json)
   );
 }
 
 getAllToDo().then((result) => {
   console.log(result);
 });
 
 
 // [Section] Getting a specific to do list item
 async function getSpecificToDo() {
   return await (
     fetch('https://jsonplaceholder.typicode.com/todos/99')
       .then((response) => response.json())
       .then((json) => json)
   );
 }
 
 getSpecificToDo().then((result) => {
   console.log(result);
 });
 
 
 // [Section] Creating a to do list item using POST method
 async function createToDo() {
   return await (
     fetch('https://jsonplaceholder.typicode.com/todos', {
       method: 'POST',
       body: JSON.stringify({
         title: 'New ToDo',
         completed: false,
         userId: 1,
       }),
       headers: {
         'Content-type': 'application/json; charset=UTF-8',
       },
     })
       .then((response) => response.json())
       .then((json) => json)
   );
 }
 
 createToDo().then((result) => {
   console.log(result);
 });
 
 
 // [Section] Updating a to do list item using PUT method
 async function updateToDo() {
   return await (
     fetch('https://jsonplaceholder.typicode.com/todos/1', {
       method: 'PUT',
       body: JSON.stringify({
         id: 1,
         title: 'Updated ToDo',
         completed: true,
         userId: 1,
       }),
       headers: {
         'Content-type': 'application/json; charset=UTF-8',
       },
     })
       .then((response) => response.json())
       .then((json) => json)
   );
 }
 
 updateToDo().then((result) => {
   console.log(result);
 });
 
 
 // [Section] Deleting a to do list item
 async function deleteToDo() {
   return await (
     fetch('https://jsonplaceholder.typicode.com/todos/1', {
       method: 'DELETE',
     })
       .then((response) => response.status)
   );
 }
 




//Do not modify
//For exporting to test.js
try{
  module.exports = {
      getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
      getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
      getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
      createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
      updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
      deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
  }
} catch(err){

}
