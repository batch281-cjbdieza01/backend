/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



console.log("Hello World!")

function getInfo() {
    let userFullName = prompt("Enter your name: ");
    let userAge = prompt("Enter your age: ");
    let userLocation = prompt("Enter your location");
    alert("Thank You! Your nformation has been saved");
    console.log("Full Name: " + userFullName);
    console.log("Age: " + userAge);
    console.log("Location: " + userLocation);
}
getInfo();


function favoriteBands() {
    console.log("My Top 5 Favorite Bands/Musical Artists:");
    console.log("1. Radiohead");
    console.log("2. Vanilla Sky");
    console.log("3. GraceNote");
    console.log("4. Phone Calls From Home");
    console.log("5. Begees");
}
favoriteBands();

function displayFavoriteMovies() {
    console.log("My Top 5 Favorite Movies :");
    console.log("1. Catch Me if You can\n   Rotten Tomatoes Rating: 96%");
    console.log("2. The Dictator\n   Rotten Tomatoes Rating: 56%");
    console.log("3. The Dark Knight\n   Rotten Tomatoes Rating: 94%");
    console.log("4. Jphn Wick 4\n   Rotten Tomatoes Rating: 94%");
    console.log("5. Zohan\n   Rotten Tomatoes Rating: 37%");
}

displayFavoriteMovies();



function printUsers() {
    let friend1,
        friend2,
        friend3;

    function printFriends() {
        alert("Hi! Please add the names of your friends.");
        friend1 = prompt("Enter your first friend's name:");
        friend2 = prompt("Enter your second friend's name:");
        friend3 = prompt("Enter your third friend's name:");

        console.log("You are friends with:");
        console.log(friend1);
        console.log(friend2);
        console.log(friend3);
    }

    printFriends();
}

printUsers();