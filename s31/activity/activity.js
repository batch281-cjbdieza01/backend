// 1. What directive is used by Node.js in loading the modules it needs?
//answer
    //`require` directives


// 2. What Node.js module contains a method for server creation?
//answer
    //`http` module


// 3. What is the method of the http object responsible for creating a server using Node.js?
//answer
    //the `createServer()` method



// 4. What method of the response object allows us to set status codes and content types?
//answer
    //`writeHead()` method
	


// 5. Where will console.log() output its contents when run in Node.js?
    //answer
        //its contents anre output to the output stream where the node.js was executed, this time, i executed/test my node.js in powershell and cmd and it works the same way as gitbash terminal


// 6. What property of the request object contains the address's endpoint?
    //answer
        //`request.url` bacause it represents the URl path portion including the query string if it is present

