console.log("Hello World!")

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function */

//Add
function add(num1, num2) {
    var result = num1 + num2;
    console.log("Displayed sum of 38 and 90:\n", result);
}
add(38, 98);

// Subtract
function subtract(num1, num2) {
    var result = num1 - num2;
    console.log("The result of the subtraction is:\n", result);
}
subtract(4300, 4500);




/*
2. Create a
function which will be able to multiply two numbers. -
    Numbers must be provided as arguments. -
    Return the result of the multiplication.

Create a
function which will be able to divide two numbers. -
    Numbers must be provided as arguments. -
    Return the result of the division.

Create a global variable called outside of the
function called product. -
    This product variable should be able to receive and store the result of multiplication
function.
Create a global variable called outside of the
function called quotient. -
    This quotient variable should be able to receive and store the result of division
function.

Log the value of product variable in the console.
Log the value of quotient variable in the console.
*/



// multiplication of two numbers
function multiplyNumbers(num1, num2) {
    return num1 * num2;
}

// division of two numbers
function divideNumbers(num1, num2) {
    return num1 / num2;
}
// global variable to store the product and quotien
let product;
let quotient;

product = multiplyNumbers(281, 145);
quotient = divideNumbers(4868, 88);

console.log("The product of 281 and 145 is: \n" + product);
console.log("The quotient of 4868, 88 is: \n" +
    quotient);



/*
3. Create a
function which will be able to get total area of a circle from a provided radius. -
    a number should be provided as an argument. -
    look up the formula
for calculating the area of a circle with a provided / given radius. -
    look up the use of the exponent operator. -
    you can save the value of the calculation in a variable. -
    return the result of the area calculation.

Create a global variable called outside of the
function called circleArea. -
    This variable should be able to receive and store the result of the circle area calculation.

Log the value of the circleArea variable in the console.*/

// Calculating Cirle Area
function circleArea(radius) {
    const pi = 3.14159; // Approximate value of pi
    const area = pi * radius ** 2;
    return area;
}
let area = circleArea(11);
console.log("The result of getting the area of the circle with 11 radius: \n")
console.log(area);


/*
4. Create a
function which will be able to get total average of four numbers. -
    4 numbers should be provided as an argument. -
    look up the formula
for calculating the average of numbers. -
    you can save the value of the calculation in a variable. -
    return the result of the average calculation.

Create a global variable called outside of the
function called averageVar. -
    This variable should be able to receive and store the result of the average calculation -
    Log the value of the averageVar variable in the console.
*/
let averageVar;

function average(num1, num2, num3, num4, ) {
    const sum = num1 + num2 + num3 + num4;
    const average = sum / 4;
    averageVar = average;
    return average;
}
let num1 = 87;
let num2 = 85;
let num3 = 90;
let num4 = 100;
const result = average(num1, num2, num3, num4);
console.log("The average of 87, 85, 90, and 100 is: ");
console.log(averageVar);


/*

5. Create a
function which will be able to check
if you passed by checking the percentage of your score against the passing percentage. -
    this
function should take 2 numbers as an argument, your score and the total score. -
    First, get the percentage of your score against the total.You can look up the formula to get percentage. -
    Using a relational operator, check
if your score percentage is greater than 75, the passing percentage.Save the value of the comparison in a variable called isPassed. -
    return the value of the variable isPassed. -
        This
function should
return a boolean.

Create a global variable called outside of the
function called isPassingScore. -
    This variable should be able to receive and store the boolean result of the checker
function. -
Log the value of the isPassingScore variable in the console.*/


function checkPassingScore(score, totalScore) {
    const percentage = (score / totalScore) * 100;
    const isPassed = percentage > 75;
    return isPassed;
}
let isPassingScore;
isPassingScore = checkPassingScore(121, 150);
console.log(isPassingScore);