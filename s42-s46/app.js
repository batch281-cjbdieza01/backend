const express = require("express");
const mongoose = require ("mongoose");
require('dotenv').config();
const morgan = require('morgan');
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes");
const productRoutes = require("./Routes/productRoutes");
const app = express();

mongoose.connect(process.env.DB_Connection, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection
db.on('error', console.error.bind(console,"MongoDB Conection error"));
db.once('open', () => console.log("Database is ready. ."));
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.options('*');
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 8080, ()=> {
    console.log(`API is now online on port ${process.env.PORT || 8080}`)
})