const Archive = require("../Models/Archives");
const User = require("../Models/User");
const Product = require('../Models/Product')
const bcrypt = require("bcrypt");
const auth = require("../auth")

// Register
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    isAdmin: true,
  });
  function geneRanBool() {
    newUser.isAdmin = !newUser.isAdmin;
    return Math.random() < 0.5;
  }
  newUser.isAdmin = geneRanBool();
  return newUser
    .save()
    .then((savedUser) => savedUser)
    .catch((error) => {
      console.error(error);
      throw error;
    });
};


// Login
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then(result => {
      if (result === null) {
        return "Invalid email or password!";
      } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        if (isPasswordCorrect) {
          const token = auth.createAccessToken(result._id);
          console.log(token); // Display the token in the terminal
          return "Success";
        } else {
          return "Invalid email or password!";
        }
      }
    })
    .catch(error => {
      console.error(error);
      throw new Error("Error retrieving user data");
    });
};

// Retrieve all users 
module.exports.getAllUsers = (req, res) => {
  User.find({ isAdmin: false }, { email: 1 })
    .then((users) => {
      res.json(users);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ message: 'Error retrieving users' });
    });
};

// Controller: getAllUsers
module.exports.getAllUsers = () => {
  return User.find({}, { password: 0, orderedProducts: 0 })
    .then(users => {
      const modifiedUsers = users.map(user => {
        return {
          ...user._doc,
          password: '******'
        };
      });
      return modifiedUsers;
    })
    .catch(error => {
      throw error;
    });
};

// Get single user
module.exports.getUser = (userId) => {
  return User.findById(userId, { password: 0, orderedProducts: 0 })
    .exec()
    .then(user => {
      return user;
    })
    .catch(error => {
      throw error;
    });
};

// Create Order
module.exports.createOrder = async (req, res) => {
  try {
    const { userId, productId } = req.body;

    const user = await User.findById(userId);
    const product = await Product.findById(productId);

    if (!user || !product) {
      return res.status(404).json({ error: 'User or product not found' });
    }

    const orderedProduct = {
      productId: product._id,
      productName: product.name,
      quantity: 1,
    };

    user.orderedProducts.push(orderedProduct);
    product.userOrders.push({
      userId: user._id,
      orderId: 234234324,
      quantity: orderedProduct.quantity, // Include the quantity field
    });
    await user.save();
    await product.save();

    return res.status(200).json({ message: 'Order created successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Error creating order' });
  }
};



// Retrieve users with orders
module.exports.getUsersWithOrders = async (req, res) => {
  try {
    const users = await User.find({ 'orderedProducts.quantity': { $gte: 1 } })
      .select('-password') 
      .populate('orderedProducts.productId', '-orderProducts'); 

    const modifiedUsers = users.map((user) => {
      const { _id, email, orderedProducts } = user;
      return {
        _id,
        email,
        orderedProducts,
      };
    });

    res.json(modifiedUsers);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};

// Retrieve my orders
module.exports.getOrderedProducts = (req, res) => {
  const userId = req.params.userId;

  User.findById(userId)
    .select('orderedProducts')
    .populate('orderedProducts.productId', '-orderProducts')
    .then((user) => {
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      const modifiedOrderedProducts = user.orderedProducts.map((product) => {
        const { _id, name, productId, price, quantity, } = product;
        return {
          _id,
          name,
          productId,
          price,
          quantity,
        };
      });

      res.json(modifiedOrderedProducts);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ message: 'Error retrieving orderedProducts' });
    });
};

// Checkout
module.exports.checkout = (req, res) => {
  const userId = req.params.userId;

  User.findById(userId)
    .select('orderedProducts')
    .populate('orderedProducts.productId')
    .then((user) => {
      if (!user) {
        return res.status(404).json({ message: 'User not found' });
      }

      let totalQuantity = 0;
      let totalPrice = 0;

      user.orderedProducts.forEach((orderedProduct) => {
        totalQuantity += orderedProduct.quantity;
        totalPrice += orderedProduct.quantity * orderedProduct.productId.price;
      });

      const checkoutData = {
        totalQuantity,
        totalPrice
      };

      res.json(checkoutData);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ message: 'Error calculating checkout' });
    });
};

