const Archive = require("../Models/Archives");
const Product = require("../Models/Product");
const User = require('../Models/User')
const auth = require("../auth");


// Create Product
module.exports.addProduct = (data) => {
  let isActive = Math.random() < 0.5;
  let price = Math.floor(Math.random() * (1000 - 300 + 1)) + 300;
  let quantity = Math.floor(Math.random() * (50 - 30 + 1)) + 30;

  let newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: price,
      quantity: quantity,
      isActive: isActive
  });
  return newProduct.save().then((product, error) => {
      if (error) {
          return false;
      } else {
          return true;
      }
  });
};

// Get all products
module.exports.getAllProducts = () => {
  return Product.find({})
    .select('name price quantity isActive') // Include only name and price fields in the query
    .then(result => {
      return result;
    });
};

// Get all active products
module.exports.getAllActive = () => {
  return Product.find({ isActive: true })
    .select("name price quantity")
    .then((result) => {
      return result;
    });
};

// Get single product
module.exports.getProductById = (productId) => {
  return Product.findById(productId)
    .then(product => {
      return product;
    })
    .catch(error => {
      throw error;
    });
};

// Archive a product
module.exports.archiveProduct = (productId) => {
  return Product.findById(productId)
    .then((product) => {
      if (!product) {
        throw new Error('Product not found');
      }

      const archiveData = {
        name: product.name,
        description: product.description,
        price: product.price
      };

      const archivedItem = new Archive(archiveData);
      product.isActive = false; // Set isActive to false

      return Promise.all([
        archivedItem.save(),
        Product.deleteOne({ _id: productId })
      ]);
    })
    .then(([archivedItem]) => {
      return archivedItem ? true : false;
    })
    .catch((error) => {
      console.error(error);
      throw error;
    });
};

// Retrieve Archived Products
module.exports.getAllArchived = async (req, res) => {
  try {
    const archives = await Archive.find();
    res.json(archives);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Activate product
module.exports.restoreProduct = (req, res) => {
  const productId = req.params.productId;

  Archive.findByIdAndRemove(productId)
    .then((archivedProduct) => {
      if (!archivedProduct) {
        return res.status(404).json({ message: 'product not found' });
      }

      const restoredProduct = new Product({
        name: archivedProduct.name,
        description: archivedProduct.description,
        price: archivedProduct.price,
        isActive: true
      });

      restoredProduct
        .save()
        .then((product) => {
          res.json(product);
        })
        .catch((error) => {
          console.error(error);
          res.status(500).json({ message: 'Failed' });
        });
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ message: 'Failed' });
    });
};

// Update product
module.exports.updateProduct = (req, res) => {
  const productId = req.params.productId;
  const updatedProduct = req.body;

  Product.findByIdAndUpdate(productId, updatedProduct, { new: true })
    .select('-orderProducts')
    .then((product) => {
      if (!product) {
        return res.status(404).json({ message: 'Product not found' });
      }
      res.json(product);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).json({ message: 'Error updating product' });
    });
};