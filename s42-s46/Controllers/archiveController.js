const Archive = require("../Models/Archives");
const Product = require("../Models/Product");
const User = require('../Models/User')
const auth = require("../auth");


// Archive a product
module.exports.archiveProduct = (reqParams) => {
    const productId = reqParams.productId;
  
    return Product.findByIdAndRemove(productId)
      .then((archivedProduct) => {
        if (!archivedProduct) {
          return false;
        }
  
        const archiveData = {
          name: archivedProduct.name,
          description: archivedProduct.description,
          price: archivedProduct.price
        };
  
        const archivedItem = new Archive(archiveData);
        return archivedItem.save();
      })
      .then((archivedItem) => {
        if (archivedItem) {
          return true;
        } else {
          return false;
        }
      });
  };



  // Get all archived products
module.exports.getAllArchived = async (req, res) => {
    try {
      const archives = await Archive.find();
      res.json(archives);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  };