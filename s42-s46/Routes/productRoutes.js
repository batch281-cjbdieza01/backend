const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController")
const auth = require("../auth");



// Create Products
router.post("/:userId/addproduct", auth.verify, (req, res) => {
    const data = {
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true) {
        productController.addProduct(data).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }
});

// Get all products
router.get('/:userid/lists', (req, res) => {
    productController.getAllProducts()
      .then(resultFromController => {

        const products = resultFromController.map(product => {
          return {
            name: product.name,
            price: product.price,
            quantity: product.quantity,
            Active: product.isActive
          };
        });
        res.send(products);
      })
      .catch(error => res.status(500).send(error));
  });

//Get all active products
router.get("/allactive", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Get single product
router.get("/product/:productId", (req, res) => {
    const productId = req.params.productId;
  
    productController.getProductById(productId)
      .then(product => {
        if (product) {
          res.json(product);
        } else {
          res.status(404).json({ error: "Product not found" });
        }
      })
      .catch(error => {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
      });
  });
  

// Archive a product
router.put('/:productId/archive', (req, res) => {
    const productId = req.params.productId;
    
    productController
      .archiveProduct(productId)
      .then((resultFromController) => res.send(resultFromController))
      .catch((error) => {
        console.error(error);
        res.status(500).send('Error archiving product');
      });
  });
  
// GET /products/archived
router.get('/archived', productController.getAllArchived);

// Arcivate/restore
  router.put('/:productId/restore', auth.verify, productController.restoreProduct);

  // Update product
router.patch('/products/:productId', auth.verify, productController.updateProduct);


module.exports = router;