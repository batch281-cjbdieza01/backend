const express = require("express");
const router = express.Router();
const User = require("../Models/User");
const userController = require("../Controllers/userController");
const auth = require("../auth");



// Register
router.post("/register", (req, res) => {
    const email = req.body.email;
  
    User.findOne({ email })
      .then((existingUser) => {
        if (existingUser) {
          res.status(400).send({ error: "Email already exists" });
        } else {
          userController.registerUser(req.body)
            .then((newUser) => {
              res.send({ id: newUser._id, message: "Success" });
            })
            .catch((error) => {
              console.error(error);
              res.status(500).send("Internal Server Error");
            });
        }
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send("Internal Server Error");
      });
  });
  


// Log-in
router.post("/login", (req, res) => {
    userController.loginUser(req.body)
      .then(resultFromController => {
        if (resultFromController === "Success") {
          res.send({ message: "Success" });
        } else {
          res.status(401).send({ error: resultFromController });
        }
      })
      .catch(error => {
        console.error(error);
        res.status(500).send("Internal Server Error");
      });
  });
  
  
// Retrieve all users 
router.get('/:userId/lists', (req, res) => {
    User.find({ isAdmin: false }, { email: 1 })
      .then((users) => {
        res.json(users);
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({ message: 'Error retrieving users' });
      });
  });



// Retrieve all users
router.get('/:userId/listsall', auth.verify, (req, res) => {
    userController.getAllUsers()
      .then(users => {
        res.json(users);
      })
      .catch(error => {
        console.error(error);
        res.status(500).json({ message: 'Error retrieving users' });
      });
  });
  

// Get Single User
  router.get('/:userId/getuser', auth.verify, (req, res) => {
    const userId = req.params.userId;
  
    userController.getUser(userId)
      .then(user => {
        if (user) {
          const modifiedUser = {
            ...user._doc,
            password: '******',
            orderedProducts: undefined
          };
          res.json(modifiedUser);
        } else {
          res.status(404).json({ message: 'User not found' });
        }
      })
      .catch(error => {
        console.error(error);
        res.status(500).json({ message: 'Error retrieving user' });
      });
  });



// // Retrieving all the products
// router.get('/all', (req, res) => {
//     productController.getAllProducts().then(resultFromController => res.send(resultFromController));
// })




// Place an order
// router.post('/order', (req, res) => {
//     let data = {
//         userId: req.body.userId,
//         productId: req.body.productId,
//     };
//     userController.order(data)
//     .then(resultFromController => res.send(resultFromController))
//     .catch(error => res.status(500).send("Error"));
// });





// Place Order
router.post('/order', userController.createOrder);


// Retrieve users with orders
router.get('/:userId/orders', auth.verify, userController.getUsersWithOrders);


/*

// Retrieve all archived items
router.get('/archive/lists', userController.getAllArchives);
// Option 2
router.get('/lists', auth.verify, userController.getAllUsers);
*/



// Retrieve my orders
router.get('/:userId/myOrders', userController.getOrderedProducts);




// Checkout
router.get('/:userId/checkout', userController.checkout);




module.exports = router;