//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
let num = 64;
const getCube = num ** 3


// Template Literals

console.log(`The cube of ${num} is ${getCube}.`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const [houseNumber, street, city, zipCode] = address;
console.log(address);
console.log(`The full address is ${houseNumber} ${street} ${city} ${zipCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;
console.log(`The animal's name is ${name}. It is a ${species} weighing ${weight} and measuring ${measurement}.`);



// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach(num => console.log(num));

const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
console.log(reduceNumber)

// Javascript Classes
class Dog {
    constructor(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
    }
}


const dog = new Dog("Tagpi, 21,rottweiler")
console.log(dog);
//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}